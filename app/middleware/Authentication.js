var jwt = require('jsonwebtoken')
var Response = require('./Response')
var Club = require('../models/club.model')

module.exports = (req,res, next) =>{

    if(req.header('Authorization')){
        jwt.verify(req.header('Authorization'), process.env.JWT_SECRET, (err,decoded)=>{
            if(err){
                Response(res,false,"cannot decode",err)

            }else{
                // res.send(decoded)
                Club.findById(decoded.id)
                .then(club=>{
                    // res.send(club)
                    if(club){
                        req.clubId = club._id
                        req.email = club.email
                        
                        next()
                    }else{
                        Response(res,false,"club not found")
                    }
                }).catch(errClub=>{
                    Response(res,false,"something went wrong from Auth",errClub)
                    console.log(req.clubId)
                })
            }
            
        })
    }else{
        console.log(req.headers.Authorization)
        Response(res, false, "token is required")
    }

}