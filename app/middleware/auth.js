const jwt = require('jsonwebtoken')
const Athlete = require('../models/athlete.model')
const jwt_pass = process.env.JWT_SECRET2
const Response = require('./Response')


const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization')
        const decoded = jwt.verify(token, jwt_pass)
        const athlete = await Athlete.findOne({ _id: decoded._id, 'tokens.token': token })

        if(!athlete) {
            throw new Error()
        }

        req.token = token
        req.athlete = athlete
        next()
    } catch (error) {
        Response(res, false, "autentikasi eror", error.message)
    }
}

module.exports = auth