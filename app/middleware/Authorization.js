var Scholarship = require('../models/scholarship.model')
var Response = require('./Response')
var Club = require('../models/club.model')

exports.scholarship = (req,res,next)=>{
    Scholarship.findById(req.params.id)
    .then(scholarship=>{
        if(String(req.clubId) == String(scholarship.club)){
            next()
        }else{
            Response(res, false, "your not authorized")
        }
        
    })
    .catch(err=>{
        Response(res,false,"something went wrong from ProductAuth", err)
    })
}

exports.club = (req,res,next)=>{
    if(req.clubId == req.params.id){
       next()
    }else{
        Response(res, false, "your not authorized")
    }
}

exports.review = async (req,res,next)=>{
    try{
       var review = await Review.findById(req.params.id)
        if(String(review.user)=== String(req.userId)){
            req.productId = review.product
            next()
        }else{
            Response(res,false,"your not authorized")
        }

}catch(err){
        Response(res,false,"error from authorization")
    }
}