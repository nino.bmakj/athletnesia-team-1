const multer = require('multer')

module.exports = upload = multer({
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, cb) {
        if(!file.originalname.match(/\.(jpg|jpeg|png)$/))
        {
            return cb(new Error('Format file tidak didukung!'))
        }

        cb(undefined, true)
    }
})