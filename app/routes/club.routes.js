var club = require('../controllers/club.controller')
var checkClub = require('../middleware/CheckClub')
var Authentication = require('../middleware/Authentication')
var Authorization = require('../middleware/Authorization')

module.exports = (app) =>{
    app.post('/club', checkClub.create, club.clubCreate)
    app.get('/club', club.clubShowAll)
    app.get('/club/:id', club.clubShow)
    app.put('/club/:id',Authentication, Authorization.club,checkClub.update, club.clubUpdate)
    app.delete('/club/:id', club.clubDelete)
    app.post('/club/login', club.clubLogin)
}