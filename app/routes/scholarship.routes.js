var scholarship = require('../controllers/scholarship.controller')
var Authentication = require('../middleware/Authentication')
var Authorization = require('../middleware/Authorization')

module.exports=(app)=>{
    app.post('/scholarship',Authentication ,scholarship.scholarshipCreate)
    app.get('/scholarship', scholarship.scholarshipShowAll)
    app.get('/scholarship/:id', scholarship.scholarshipShow)
    app.delete('/scholarship/:id', scholarship.scholarshipDelete) //Authentication, Authorization.scholarship,
    app.put('/scholarship/:id',Authentication,Authorization.scholarship, scholarship.scholarshipUpdate)


    // app.get('/scholarship')
    app.put('/scholarship/:id/portofolio/:idPortofolio', Authentication, Authorization.scholarship, scholarship.clubConfirmation)
}