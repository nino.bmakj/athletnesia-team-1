const athlete = require('../controllers/athlete.controller')
const auth = require('../middleware/auth')
const upload = require('../middleware/upload')

module.exports = (app) => {
    app.post('/athlete', athlete.athleteCreate)
    app.post('/athlete/login', athlete.athleteLogin)
    app.post('/athlete/logout', auth, athlete.athleteLogout)
    app.post('/athlete/logoutall', auth, athlete.athleteLogoutAll)
    app.get('/athlete', athlete.athleteShowAll)
    // app.get('/athlete/:id', athlete.athleteShow)
    app.get('/athlete/me', auth, athlete.athleteShowMe)
    app.patch('/athlete/me', auth, athlete.athleteUpdate)
    app.delete('/athlete/me', auth, athlete.athleteDelete)

    app.post('/athlete/me/image', auth, upload.single('image'), athlete.athleteAddImage)
    app.delete('/athlete/me/image', auth, athlete.athleteDeleteImage)
    app.get('/athlete/:id/image', athlete.athleteShowImage)
}