const portofolio = require('../controllers/portofolio.controller')
const auth = require('../middleware/auth')
const authPortofolio = require('../middleware/authPortofolio')

module.exports = (app) => {
    app.post('/portofolio', auth, portofolio.portofolioCreate)
    app.get('/portofolio', portofolio.portofolioShowAll)
    app.get('/portofolio/:id', portofolio.portofolioShow)
    app.patch('/portofolio/:id', auth, portofolio.portofolioUpdate)
    app.delete('/portofolio/:id', auth, portofolio.portofolioDelete)
    app.put('/apply/:id', authPortofolio, portofolio.applyPortofolio)
}