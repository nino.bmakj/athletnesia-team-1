const mongoose = require('mongoose')
const validator = require('validator')

var clubSchema = new mongoose.Schema({
    name:{type:String, required:true, trim:true, unique:true},
    cabangOlahraga:{type: String, default:''},
    image:{type:String, default:''},
    address:{type:String, default:''},
    telepon:{type:String, default:''},
    password:{type:String, required:true, trim:true},
    email:{
        type:String, required:true, trim:true, unique:true,
        validate(value) {
            if(!validator.isEmail(value)) {
                throw new Error('Email tidak valid!')
            }
        }
    },
    aboutUs:{type:String, default:''},
    scholarship:[{type:mongoose.Schema.Types.ObjectId, ref:'scholarship'}],
    athlete:[{type:mongoose.Schema.Types.ObjectId, ref:'athlete'}],
})

var Club = mongoose.model('club', clubSchema)

module.exports = Club