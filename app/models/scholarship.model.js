const mongoose = require('mongoose')

var scholarshipSchema = new mongoose.Schema({
    scholarshipTitle:{type:String, required:true},
    kuota:{type:Number, required:true},
    description:{type:String, required:true},
    syarat:{type:String, required:true},
    startDate:{type:Date, default:Date.now},
    endDate:{type:Date, default:Date.now},
    club:{type:mongoose.Schema.Types.ObjectId, ref:'club'},
    portofolio:[{type:mongoose.Schema.ObjectId, ref:'Portofolio'}], 
    status:{type:String, default:''}
    
})
var Scholarship = mongoose.model('scholarship', scholarshipSchema)

module.exports = Scholarship