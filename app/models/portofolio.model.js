const mongoose = require('mongoose')

const portofolioSchema = new mongoose.Schema({
    dateOfBirth: {
        type: Date,
        required: [true, "tanggal lahir tidak boleh kosong"]
    },
    image: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: [true, "alamat tidak boleh kosong"]
    },
    cabangOlahraga: {
        type: String,
        required: [true, "cabang olahraga tidak boleh kosong"]
    },
    prestasi: [{
        type: String,
        default: ''
    }],
    video: {
        type: String,
        default: ''
    },
    deskripsi: {
        type: String,
        required: [true, "deskripsi tidak boleh kosong"]
    },
    skill : [{
        type: String,
        required: [true, "skill tidak boleh kosong"]
    }],
    athlete: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Athlete'
    },
    scholarship:[{type:Object,
    ref:'scholarship'}]
})

const Portofolio = mongoose.model('Portofolio', portofolioSchema)

module.exports = Portofolio
