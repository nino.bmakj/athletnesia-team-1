const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const jwt_pass = process.env.JWT_SECRET2

const athleteSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "nama tidak boleh kosong"],
        trim: true
    },
    email: {
        type: String,
        required: [true, "email tidak boleh kosong"],
        unique: true,
        trim: true,
        lowercase: true,
        validate(value) {
            if(!validator.isEmail(value)) {
                throw new Error('Email tidak valid')
            }
        }
    },
    password: {
        type: String,
        required: [true, "password tidak boleh kosong"],
        trim: true,
        validate(value) {
            if (value.toLowerCase().includes("password")) {
                throw new Error('Password tidak boleh ada kata "password"')
            }
        }
    },
    image: {
        type: Buffer
    },
    tokens: [{
        token: {
        type: String,
        required: true}
    }],
    portofolio: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Portofolio'
    }]
})

// to hide password and tokens from /GET/athlete
athleteSchema.methods.toJSON =function () {
    const athlete = this
    const athleteObject = athlete.toObject()

    delete athleteObject.password
    delete athleteObject.tokens
    delete athleteObject.image

    return athleteObject
}

athleteSchema.methods.generateAuthToken = async function() {
    const athlete = this
    const token = jwt.sign( { _id: athlete._id.toString() }, jwt_pass)

    athlete.tokens = athlete.tokens.concat({ token })
    await athlete.save()

    return token
}

athleteSchema.statics.findByCredentials = async (email, password) => {
    const athlete = await Athlete.findOne({ email })

    if (!athlete) {
        throw new Error('password salah atau email belum terdaftar')
    }

    const isMatch = await bcrypt.compare(password, athlete.password)

    if (!isMatch) {
        throw new Error('password salah atau email belum terdaftar')
    }

    return athlete
}

athleteSchema.pre('save', async function (next) {
    const athlete = this

    if (athlete.isModified('password')) {
        athlete.password = await bcrypt.hash(athlete.password, 8) 
    }

    next()
})

const Athlete = mongoose.model('Athlete', athleteSchema)
module.exports = Athlete