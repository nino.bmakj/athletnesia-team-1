require('dotenv').config()
var Club = require('../models/club.model')
const bcrypt = require('bcrypt')
var Response = require('../middleware/Response')
var jwt = require('jsonwebtoken')

exports.clubCreate = (req, res) => {
    var newClub = new Club({
        name: req.body.name,
        cabangOlahraga: req.body.cabangOlahraga,
        password: bcrypt.hashSync(req.body.password, parseInt(process.env.BCRYPT_SALT)),
        email: req.body.email,
        cabangOlahraga: req.body.cabangOlahraga,
        address: req.body.address,
        telepon: req.body.telepon,
        image: req.body.image,
        aboutUs: req.body.aboutUs
    })
    newClub.save()
        .then(createdClub => {
            var token = jwt.sign({
                email: createdClub.email,
                id: createdClub._id
            }, process.env.JWT_SECRET)
            Response(res, true, 'Club Company Created', {createdClub, token})
        }).catch(err => {
            Response(res, false, 'cannot create Club Company', err)
        })
}

exports.clubShowAll = (req, res) => {
    Club.find({})
        .then(allclub => {
            Response(res, true, 'All Club is retrieved', allclub)
        }).catch(err => {
            Response(res, false, 'cannot get all club', err)
        })
}

exports.clubShow = (req, res) => {
    Club.findById(req.params.id)
        .populate({
            path: 'scholarship',
            select: ['scholarshipTitle',
                'kuota', 'syarat', 'startDate', 'endDate']
        })
        .then(club => {
            if(club){
                Response(res, true, 'club is retrieved', club)
            }else{
                Response(res, false, 'cannot get club 1', err)
            }
            
        }).catch(err => {
            Response(res, false, 'cannot get club 2', err)
        })
}

exports.clubDelete = (req, res) => {
    Club.findByIdAndRemove(req.params.id, { useFindAndModify: false })
        .then(data => {
            console.log(data)
            if (data) { Response(res, true, 'Club Deleted', data) }
            else { Response(res, false, 'cannot delete club 1', err) }
        }).catch(err => {
            
            Response(res, false, 'cannot delete club 2', err)
        })
}

exports.clubUpdate = (req, res) => {
    var updateClub = req.body
    if (updateClub.password) {
        updateClub.password = bcrypt.hashSync(req.body.password, 10)
    }
    Club.findByIdAndUpdate(req.params.id, {
        $set: updateClub
    }, {
            new: true,
            useFindAndModify: false
        }).then(updated => {
            Response(res, true, 'club update', updated)
        }).catch(err => {
            Response(res, false, 'cannot update club', err)
        })
}

exports.clubLogin = (req, res) => {
    Club.findOne({ email: req.body.email })
        .then(club => {
            var hash = bcrypt.compareSync(req.body.password, club.password)
            if (hash) {
                var token = jwt.sign({
                    email: club.email,
                    id: club._id
                }, process.env.JWT_SECRET)
                Response(res, true, 'your logged in', { token: token, clubId: club._id })
            } else { Response(res, false, "wrong Password") }
        }).catch(err => {
            Response(res, false, 'cannot log in', "err")
        })
}
