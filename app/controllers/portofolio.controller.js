const Portofolio = require('../models/portofolio.model')
const Athlete = require('../models/athlete.model')
const Response = require('../middleware/Response')
const Scholarship = require('../models/scholarship.model')
var MailConfig = require('../config/email');
var hbs = require('nodemailer-express-handlebars');
var gmailTransport = MailConfig.GmailTransport;

exports.portofolioCreate = async (req, res) => {
    if (req.athlete.portofolio[0]) {
        Response(res, false, "kamu sudah pernah membuat portofolio!")
    } else {
        const portofolio = new Portofolio({
            ...req.body,
            athlete: req.athlete._id
        })
        try {
            await portofolio.save()
            const athlete = await Athlete.findById(portofolio.athlete)
            athlete.portofolio = portofolio
            athlete.save()
            Response(res, true, "portofolio berhasil dibuat", { portofolio: portofolio, portofolioId: portofolio._id })
        } catch (error) {
            Response(res, false, "gagal membuat portofolio", error.message)

        }
    }
}

exports.portofolioShowAll = async (req, res) => {
    Portofolio.find({})
        .populate({
            path: 'athlete',
            select: ['name']
        })
        .then(all => {
            Response(res, true, "menampilkan semua portofolio", all)
        }).catch((error) => {
            Response(res, false, "gagal menampilkan portofolio", error.message)
        })
}

exports.portofolioShow = async (req, res) => {
    const portofolio = await Portofolio.findById(req.params.id)
        .populate({
            path: 'athlete',
            select: ['name', 'image']
        })

    Response(res, true, "menampilkan portofolio", portofolio)
}

exports.portofolioUpdate = async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['dateOfBirth', 'address', 'cabangOlahraga', 'prestasi', 'video', 'deskripsi', 'skill']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        return Response(res, false, 'invalid updates!', error.message)
    }

    try {
        const portofolio = await Portofolio.findOne({ _id: req.params.id, athlete: req.athlete._id })

        if (!portofolio) {
            return Response(res, false, "tidak ditemukan")
        }
        updates.forEach((update) => portofolio[update] = req.body[update])
        await portofolio.save()
        Response(res, true, "portofolio berhasil diupdate", portofolio)

    } catch (error) {
        Response(res, false, "tidak bisa update", error.message)
    }
}

exports.portofolioDelete = (req, res) => {
    Portofolio.findByIdAndRemove(req.params.id, { useFindAndModify: false })
        .then(portofolio => {
            Athlete.findByIdAndUpdate(req.athlete._id, { $pull: { portofolio: { $in: portofolio._id } } })
                .then(() => {
                    Response(res, true, "portofolio berhasil dihapus", portofolio)
                }).catch((error) => {
                    Response(res, false, "gagal menghapus portofolio", error.message)
                })
        }).catch((error) => {
            Response(res, false, "gagal menghapus portofolio", error.message)
        })
}

exports.applyPortofolio = (req, res) => {
    //var portofolio = `{"portofolio":"${req.portofolio}"}`
    //var scholarship = `{"scholarship":"${req.params.id}"}`
    Scholarship.findOneAndUpdate({ "_id": req.params.id }, {
        $set: { portofolio: req.portofolio }
    }, { new: true })
        .then(scholarship => {
            console.log(scholarship)
            Portofolio.findOneAndUpdate({ "_id": req.portofolio }, {
                $set: { scholarship: { scholarshipId: req.params.id, status: 'waiting confirmation' } }
            }, { new: true })
                .then(data => {
                    Response(res, true, 'apply scholarship', data)
                    Athlete.findById(req.athlete._id)
                    .then(athlete =>{ 
                            MailConfig.ViewOption(gmailTransport,hbs);
                            let HelperOptions = {
                              from: '"Atletnesia" <atletnesia.binar@gmail.com>',
                              to: athlete.email,
                              //to: "muhammad.farokhi96@gmail.com",
                              subject: 'Please Waiting Confirmation',
                              //template: 'test',
                              html:` <div className="HomeContainerAll">
                              <div className="HomeContainerAtas">
                              
                              <div className="KiniSaatnya" style="margin: 161px 534px 10px 135px; margin-top: 161px; margin-right: 534px; margin-bottom: 10px; margin-left: 135px;">
                                  <h1 style="width: 697px;
                                  height: 202px;
                                  font-family: Product Sans;
                                  font-size: 50px;
                                  font-weight: bold;
                                  font-stretch: normal;
                                  font-style: normal;
                                  line-height: 1.5;
                                  letter-spacing: 1.2px;
                                  text-align: left;
                                  color: #707070;">Terima Kasih sudah melakukan Apply</h1>
                                   <h1 style="width: 697px;
                                   height: 202px;
                                   font-family: Product Sans;
                                   font-size: 50px;
                                   font-weight: bold;
                                   font-stretch: normal;
                                   font-style: normal;
                                   line-height: 1.5;
                                   letter-spacing: 1.2px;
                                   text-align: left;
                                   color: #707070;">Tunggu konfirmasi dari Club</h1>
                      
                              </div>
                              <div className=" GambarPojok" style="width: 712px;
                              height: 712.2px;
                              object-fit: contain;
                              position: absolute;
                              float: unset;
                              top: 0px;
                              right: 0px;
                              z-index: -1;">
                                  <img alt="artboard" src="https://ik.imagekit.io/atletnesia/artboard1_VkTd-hoC4.png"/>
                              </div>
                          </div>
                          </div>
                      
                      `,
                              context: {
                                name:"Atletnesia",
                                email: "atletnesia.binar@gmail.com",
                                address: "The Breeze BSD Tangerang Selatan"
                              }
                            };
                            gmailTransport.sendMail(HelperOptions, (error,info) => {
                              if(error) {
                                console.log(error);
                                //res.json(error);
                              }else if(!error){
                                console.log("email is send");
                              //console.log(info);
                              //res.json(info)
                              }
                              
                            });
                        
                            
                    }).catch(err=>{
                        console.log(err)
                    })
                }).catch(err => {
                    Response(res, false, 'error in find id portofolio', err)
                })
        })
        .catch(err => {
            Response(res, false, 'cannot apply', err)
        })
}



// exports.showPortofolioByScholarship = (req, res) => {
//     Portofolio.find({ "scholarship": ObjectId(`${req.params.id}`) })
//         .then(portofolio => {
//             Response(res, true, "Portofolio Retrieved", portofolio)
//         }).catch(err => {
//             Response(res, false, "error to find Portofolio", err)
//         })
// }

