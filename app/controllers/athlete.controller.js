const Athlete = require('../models/athlete.model')
const Response = require('../middleware/Response')
const Portofolio = require('../models/portofolio.model')
const sharp = require('sharp')

exports.athleteCreate = async (req, res) => {
    const athlete = new Athlete(req.body)

    try {
        await athlete.save()
        const token = await athlete.generateAuthToken()
        Response(res, true, "akun berhasil dibuat", { athlete, token })
    } catch (error) {
        Response(res, false, "akun tidak berhasil dibuat", error.message)
    }
}

exports.athleteLogin = async (req, res) => {
    try {
        const athlete = await Athlete.findByCredentials(req.body.email, req.body.password)
        const token = await athlete.generateAuthToken()
        const porto = await Portofolio.findById(athlete.portofolio)
        const cabang = await porto.cabangOlahraga
        Response(res, true, "berhasil login", { token: token, id: athlete._id, portofolioId: athlete.portofolio, cabang })
    } catch (error) {
        Response(res, false, "gagal login", error.message)
    }
}

exports.athleteLogout = async (req, res) => {
    try {
        req.athlete.tokens = req.athlete.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.athlete.save()
        Response(res, true, "berhasil log out")
    } catch (error) {
        Response(res, false, "gagal logout", error.message)
    }
}

exports.athleteLogoutAll = async (req, res) => {
    try {
        req.athlete.tokens = []
        await req.athlete.save()
        Response(res, true, "berhasil log out dari semua device")
    } catch (error) {
        Response(res, false, "gagal logout", error.message)
    }
}

exports.athleteShowAll = (req, res) => {
    Athlete.find({}).then(all => {
        Response(res, true, "menampilkan semua atlit", all)
    }).catch((error) => {
        Response(res, false, "gagal menampilkan atlit", error)
    })
}

exports.athleteShow = async (req, res) => {
    try {
        const athlete = await Athlete.findById(req.params.id)
        Response(res, true, "menampilkan atlit", athlete)

    } catch (error) {
        Response(res, false, "tidak ditemukan", error.message)
    }
}

exports.athleteShowMe = async (req, res) => {
    Response(res, true, "menampilkan akun saya", req.athlete)
}

exports.athleteUpdate = async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['name', 'email', 'password', 'image']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        return Response(res, false, "update tidak valid", error)
    }

    try {
        updates.forEach((update) => req.athlete[update] = req.body[update])
        await req.athlete.save()
        Response(res, true, "update berhasil", req.athlete)
    } catch (error) {
        Response(res, false, "update gagal", error.message)
    }
}

exports.athleteDelete = async (req, res) => {
    try {
        const athlete = await req.athlete.remove()
        await Portofolio.findOneAndDelete({ athlete: athlete })
        Response(res, true, "akun dihapus", req.athlete)
    } catch (error) {
        Response(res, false, "gagal menghapus akun", error)
    }
}

exports.athleteAddImage = async (req, res) => {
    const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()
    req.athlete.image = buffer
    await req.athlete.save()
    Response(res, true, "upload image berhasil", buffer)
}, (error, req, res, next) => {
    Response(res, false, "upload image gagal", error.message)
}

exports.athleteDeleteImage = async (req, res) => {
    try {
        req.athlete.image = undefined
        await req.athlete.save()
        Response(res, true, "Foto berhasil dihapus")
    } catch (error) {
        Response(res, false, "gagal menghapus foto", error.message)
    }
}

exports.athleteShowImage = async (req, res) => {
    try {
        const athlete = await Athlete.findById(req.params.id)

        if (!athlete || !athlete.image) {
            throw new Error
        }

        res.set('Content-Type', 'image/png')
        res.send(athlete.image)
    } catch (error) {
        Response(res, false, "image tidak ditemukan", error.message)
    }
}