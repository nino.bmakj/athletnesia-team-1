var Scholarship = require('../models/scholarship.model')
var Club = require('../models/club.model')
var Response = require('../middleware/Response')

var Portofolio = require('../models/portofolio.model')

exports.scholarshipCreate = (req, res) => {
    Scholarship.create({
        scholarshipTitle: req.body.scholarshipTitle,
        kuota: req.body.kuota,
        description: req.body.description,
        syarat: req.body.syarat,
        startDate: req.body.startDate,
        endDate: req.body.endDate,
        club: req.clubId
    }).then(scholarship => {
        Club.findById(req.clubId)
            .then(club => {
                console.log(club)
                club.scholarship.push(scholarship._id)
                club.scholarship[0] == null ? club.scholarship.splice(0, 1) : null
                club.save()
                Response(res, true, 'scholarship created', scholarship)
            }).catch(err => {
                Response(res, false, 'something went wrong', err)
            })
    }).catch(err => {
        Response(res, false, "cannot create scholarship", err)
    })

}

exports.scholarshipShowAll = (req, res) => {

    Scholarship.find({})
        .populate({
            path: 'club',
            select: ['name', 'address', 'image']
        })
        .then(scholarship => {
            Response(res, true, 'scholarship retrived', scholarship)
        }).catch(err => {
            Response(res, false, 'cannot find scholarship', err)
        })
}

exports.scholarshipShow = (req, res) => {
    Scholarship.findById(req.params.id)
        .populate({
            path: 'club',
            select: ['name', 'cabangOlahraga', 'address', 'image']
        }).then(scholarship => {
            Response(res, true, 'scholarship retrieved', scholarship)
        }).catch(err => {
            Response(res, false, 'cannot find scholarship', err)
        })
}

exports.scholarshipDelete = (req, res) => {
    Scholarship.findByIdAndRemove(req.params.id, {
        useFindAndModify: false
    })
        .then(scholarship => {
            Club.findByIdAndUpdate(req.userId, { $pull: { scholarship: { $in: scholarship._id } } })
                .then(() => {
                    Response(res, true, 'scholarship deleted'.scholarship)
                }).catch(errClub => {
                    Response(res, false, 'something went wrong from handler Club', errClub)
                })
        }).catch(err => {
            Response(res, false, 'something went from handler', err)
        })
}

exports.scholarshipUpdate = (req, res) => {
    Scholarship.findByIdAndUpdate(req.params.id, {
        $set: req.body
    }, { new: true })
        .then(scholarship => {
            Response(res, true, 'scholarship', scholarship)
        })
        .catch(err => {
            Response(res, false, 'cannot update scholarship', err)
        })
}

// exports.showScholarshipByPortofolio = (req, res) =>{
//     Scholarship.find({"portofolio": ObjectId(`${req.params.id}`)})
//     .then(scholarship =>{
//         Response(res, true, "Scholarship Retrieved", scholarship)
//     }).catch(err =>{
//         Response(res, false, "error to find Portofolio", err)
//     })
// }

exports.clubConfirmation = (req, res) => {
    var data = {}
    Portofolio.findOneAndUpdate({ "_id": req.params.idPortofolio }
        , { $set: { "scholarship.$[id].status": 'finish' } }, { arrayFilters: [{ "id.scholarshipId": { $gte: req.params.id } }] })
        .then(data => {
            console.log(req.params.idPortofolio, req.params.id)

            Response(res, true, "status changed", data)
        }).catch(err => {
            console.log(req.params.idPortofolio, req.params.id)
            Response(res, false, "status not change", err)
        })

}

