require('dotenv').config()
var express = require('express')
var app = express()
var PORT = process.env.PORT || 3030
const bodyParser = require('body-parser')

const config_server = process.env.DB_ATLAS_MONGO || process.env.DB_LOCAL_MONGO
var cors = require('cors')

const mongoose = require('mongoose');
mongoose.connect(config_server, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
 });

app.get('/',(req, res)=>{
    res.send('this is atletnesia')
    
})

app.post('/request', (req,res)=>{
    res.send({
        query: req.query.apaaja
    })
})
app.use(cors())
app.use(bodyParser.json())

//API
require('./app/routes/club.routes')(app)
require('./app/routes/scholarship.routes')(app)
require('./app/routes/athlete.routes')(app)
require('./app/routes/portofolio.routes')(app)

app.listen(PORT, ()=>{
    console.log('Listening on port '+ PORT )
})